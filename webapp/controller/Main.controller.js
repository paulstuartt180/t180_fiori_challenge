sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/model/json/JSONModel",
		"T180/fiorichallenge/model/models",
		"sap/m/MessageToast"
	],

	function (Controller, JSONModel, models, MessageToast) {
		"use strict";

		return Controller.extend("T180.fiorichallenge.controller.Main", {
			onInit: function () {},

			onAfterRendering: function () {
				// Instantiate the Asset Review Model from the models.js template, set it the main view
				this.AssetReview = new JSONModel(models.createAssetReviewModelTemplate());
				this.getView().setModel(this.AssetReview, "AssetReviewModel");

				// Example; setting the 'CurrentDate' property in the Asset Review model
				this.getView().getModel("AssetReviewModel").setProperty("/CurrentDate", new Date());

				// Set the Total Reviews Counter in the Model
				var totalReviews = this.getView().getModel("AssetReviewModel").getProperty("AssetReviewModel/Reviews").length;
				this.getView().getModel("AssetReviewModel").setProperty("/TotalNumberOfReviews", totalReviews);
			},

			// Instantiate the Fragment instance, set data and open from on click event
			// Use the addDependent
			openDialogReview: function () {
				var oView = this.getView();
				// Set Temp Data Object for the Dialog Data
				this.getView().getModel("AssetReviewModel").setProperty("/newReview", models.createNewReviewTemplate());
				if (!this.newReviewDialog) {
					// Instantiate the Dialog Fragment
					this.newReviewDialog = sap.ui.xmlfragment("T180.fiorichallenge.view.addReview", this);
					oView.addDependent(this.newReviewDialog);
					this.newReviewDialog.open();
				}
				this.newReviewDialog.open();
			},

			cancelBtn: function () {
				MessageToast.show("Review Cancelled");
				this.newReviewDialog.close();
			},

			// Save Dialog Data, use setProperty() and push to the AssetReview Model
			saveBtn: function () {
				var assetReviewModel = this.getView().getModel("AssetReviewModel");

				//Getting the Reviews array from the assetReviewModel, and inserting the submitted review
				var reviewsArray = assetReviewModel.getProperty("/Reviews");
				reviewsArray.push(assetReviewModel.getProperty("/newReview"));

				// Write the updated array back to the model
				this.getView().getModel("AssetReviewModel").setProperty("/Reviews", reviewsArray);
				MessageToast.show("Review Saved");
				this.newReviewDialog.close();
			}

		});
	});