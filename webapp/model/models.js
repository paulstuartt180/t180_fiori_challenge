sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";
	
	// var DateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern: "EEE, MMM d, yyyy"});

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		

		// Asset review criteria;
		// Suitability (how well does the asset meet the business requirements)
		// Value (how much value does the asset provide in relation to its cost)
		// Durability (how reliable is the asset, how long does it last before becoming defective)
		// Availability (is the asset under heavy use, do we need more?)
		// Longevity (will the asset be useful in the future)
		createAssetReviewModelTemplate: function () {
			return {
				TotalNumberOfReviews: 0,
				CurrentDate: new Date().getDate().toString(),
				Reviews: [{
					AssetName: "X Series Forklift",
					Suitability: 5,
					SuitabilityComment: "suitable comment",
					Value: 5,
					ValueComment: "comment here",
					Durability: 5,
					DurabilityComment: "comment here",
					Longevity: 5,
					LongevityComment: "comment here",
					SummaryComments: "Smart machinery that helps us get the job done.",
					SubmissionDate: "2022-02-06",
					SubmittedBy: "Robert Jones"
				}, {
					AssetName: "Drone Quadcopter",
					Suitability: 2,
					SuitabilityComment: "We haven't found much use for this yet.",
					Value: 2,
					ValueComment: "This asset was pretty expensive.",
					Durability: 3.5,
					DurabilityComment: "comment here",
					Longevity: 3.5,
					LongevityComment: "comment here",
					SummaryComments: "Really bad",
					SubmissionDate: "2022-02-01",
					SubmittedBy: "Layla Parker"
				}, {
					AssetName: "X Series Forklift",
					Suitability: 5,
					SuitabilityComment: "Very helpful around the warehouse.",
					Value: 5,
					ValueComment: "Reasonably priced.",
					Durability: 5,
					DurabilityComment: "comment here",
					Longevity: 5,
					LongevityComment: "comment here",
					SummaryComments: "good product for jobs",
					SubmissionDate: "2022-01-08",
					SubmittedBy: "Layla Parker"
				}, {
					AssetName: "X Series Forklift",
					Suitability: 2,
					SuitabilityComment: "suitable comment",
					Value: 2,
					ValueComment: "comment here",
					Durability: 5,
					DurabilityComment: "comment here",
					Longevity: 2,
					LongevityComment: "comment here",
					SummaryComments: "It's too hard to drive.",
					SubmissionDate: "2022-01-02",
					SubmittedBy: "Ronald McDoogle"
				}, {
					AssetName: "4G63 Engine",
					Suitability: 5,
					SuitabilityComment: "suitable comment",
					Value: 5,
					ValueComment: "comment here",
					Durability: 5,
					DurabilityComment: "comment here",
					Longevity: 5,
					LongevityComment: "comment here",
					SummaryComments: "excllent choice",
					SubmissionDate: "2022-02-06",
					SubmittedBy: "Robert Jones"
				}, {
					AssetName: "4G63 Engine",
					Suitability: 4,
					SuitabilityComment: "suitable comment",
					Value: 4,
					ValueComment: "comment here",
					Durability: 5,
					DurabilityComment: "comment here",
					Longevity: 5,
					LongevityComment: "comment here",
					SummaryComments: "Really liked this",
					SubmissionDate: "2022-02-07",
					SubmittedBy: "James Smith"
				}, {
					AssetName: "Ferrari F90",
					Suitability: 5,
					SuitabilityComment: "This asset is suitable for the job.",
					Value: 4,
					ValueComment: "Expensive but high quality parts.",
					Durability: 4,
					DurabilityComment: "Requires significant maintenance but fairly reliable.",
					Longevity: 4,
					LongevityComment: "comment here",
					SummaryComments: "Now we can get to work faster.",
					SubmissionDate: "2021-11-24",
					SubmittedBy: "James Smith"
				}, {
					AssetName: "X Series Forklift",
					Suitability: 5,
					SuitabilityComment: "suitable comment",
					Value: 5,
					ValueComment: "This was well worth the investment.",
					Durability: 4,
					DurabilityComment: "comment here",
					Longevity: 4,
					LongevityComment: "Even if a new model comes out, this will still be useful.",
					SummaryComments: "cost a lot but worth it",
					SubmissionDate: "2021-11-18",
					SubmittedBy: "Sarah Rose"
				}, {
					AssetName: "Ferrari F90",
					Suitability: 3,
					SuitabilityComment: "Why do we need a Ferrari for the warehouse?",
					Value: 2,
					ValueComment: "comment here",
					Durability: 4,
					DurabilityComment: "comment here",
					Longevity: 4,
					LongevityComment: "comment here",
					SummaryComments: "over-rated",
					SubmissionDate: "2021-11-18",
					SubmittedBy: "Sarah Rose"
				}],
				Assets: [{
					"AssetName": "Drone Quadcopter",
					"icon": "sap-icon://BusinessSuiteInAppSymbols/icon-3d"
				}, {
					"AssetName": "X-Series Forklift",
					"icon": "sap-icon://BusinessSuiteInAppSymbols/icon-forklift"
				}, {
					"AssetName": "Ferrari F90",
					"icon": "sap-icon://car-rental"
				}, {
					"AssetName": "4G63 Engine",
					"icon": "sap-icon://technical-object"
				}],
				Reviewers: [{
					"ReviewerName": "Robert Jones"
				}, {
					"ReviewerName": "Layla Parker"
				}, {
					"ReviewerName": "Ronald McDoogle"
				}, {
					"ReviewerName": "James Smith"
				}, {
					"ReviewerName": "Sarah Rose"
				}],
				Ratings: [{
					"ratingScore": "0"
				}, {
					"ratingScore": "1"
				}, {
					"ratingScore": "2"
				}, {
					"ratingScore": "3"
				}, {
					"ratingScore": "4"
				}, {
					"ratingScore": "5"
				}, {
					"ratingScore": "6"
				}, {
					"ratingScore": "7"
				}, {
					"ratingScore": "8"
				}, {
					"ratingScore": "9"
				}, {
					"ratingScore": "10"
				}],
				newReview: {}
			};
		},
		createNewReviewTemplate: function () {
			return {
				AssetName: "",
				Suitability: 0,
				SuitabilityComment: "",
				Value: 0,
				ValueComment: "",
				Durability: 0,
				DurabilityComment: "",
				Longevity: 0,
				LongevityComment: "",
				SummaryComments: "",
				SubmissionDate: "",
				SubmittedBy: ""
			};
		}
	};
});